from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://learn.javascript.ru")

sun_icon = driver.find_element(By.XPATH, "/html/body/div[1]/div[1]/div[1]/div[6]/div/label/span[1]")
sun_icon.click()

background = driver.find_element(By.XPATH, "/html/body")
assert background.value_of_css_property('background-color') == "rgba(255, 255, 255, 1)"

moon_icon = driver.find_element(By.XPATH, "/html/body/div[1]/div[1]/div[1]/div[6]/div/label/span[2]")
moon_icon.click()

background_black = driver.find_element(By.XPATH, "/html/body")
assert background_black.value_of_css_property('background-color') == "rgba(35, 37, 41, 1)"