## 1

| Test Tour    | SI-1                 |
|--------------|----------------------|
| To be tested | Registration process |
| Duration     | 60s                  |
| Tester       | Nikita Morozov       |

| № | What done                                              | Status | Comment |
|---|--------------------------------------------------------|--------|---------|
| 1 | Clicked at the icon of person                          | OK     |         |
| 2 | Popup with Sign In and Sign Up options opened          | OK     |         |
| 3 | Pressed Sign Up button                                 | OK     |         |
| 4 | Form for Sign Up opened                                | OK     |         |
| 5 | Put incorrect email, it showed error                   | OK     |         |
| 6 | Put correct data                                       | OK     |         |
| 7 | Got verification email                                 | OK     |         |
| 8 | Verified email address by clicking on the link         | OK     |         |
| 9 | Was able to SIgn In with credentials I provided before | OK     |         |

## 2

| Test Tour    | SI-2            |
|--------------|-----------------|
| To be tested | Language change |
| Duration     | 10s             |
| Tester       | Nikita Morozov  |

| № | What done                                      | Status | Comment |
|---|------------------------------------------------|--------|---------|
| 1 | Clicked at the icon of planet                  | OK     |         |
| 2 | It showed list of the languages                | OK     |         |
| 3 | Clicked at the English                         | OK     |         |
| 4 | Language at the website changed to the English | OK     |         |
| 5 | Also, succesfully changed language to Chinese  | OK     |         |

## 3

| Test Tour    | SI-3               |
|--------------|--------------------|
| To be tested | Change color theme |
| Duration     | 10s                |
| Tester       | Nikita Morozov     |

| № | What done                    | Status | Comment |
|---|------------------------------|--------|---------|
| 1 | Clicked at the icon of sun   | OK     |         |
| 2 | Color theme changed to light | OK     |         |
| 3 | Clicked at the icon of moon  | OK     |         |
| 4 | Color theme changed to dark  | OK     |         |
